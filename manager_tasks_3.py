from datetime import date, datetime, timedelta
import datetime



print()
user_choice = 1
tasks = []

class Tasks:
    done = False

    def __init__(self, content_task, datetime):
        self.datetime = datetime
        self.content_tasks = content_task

    def __str__(self): #potrzebne do fizycznego przedstawienia typu obiek w konsoli przy prinotowaniu zadań
      return "Task: " + self.content_tasks + " Deadline: " + str(self.datetime)   

    def complete(self):
        if self.complete == True:
            return "Mission complete!"
        else:
            return "Not made!"


def show_tasks():
    task_index = 0

    for task in tasks:
        # date = datetime.date.today()
        # print(date)
        print(datetime.datetime.now())
        print(task)


def add_task():
    when = ["Today", "Tomarrow", "Other date"]
    content_task = input("Enter the content of task: ")
    print("What deadline do U want to set for this task?")
    for number, deadline in enumerate(when):
        print(f"{number}. Deadline for this task is{deadline}")
    while True:
        try:
            choose_date = int(input("Choose the date:"))
            if choose_date > len(when):
                raise ValueError
        except:
            print("It must be a number")
            continue

        if choose_date < len(when):
            if choose_date == 0:
                today = date.today()
                year = today.year
                month = today.month
                day = today.day
            elif choose_date == 1:
                tomarrow = date.today() + timedelta(days=1)
                year = tomarrow.year
                month = tomarrow.month
                day = tomarrow.day
            elif choose_date == 2:
                userDate = set_a_date()
                print(userDate.strftime("%x")) #print który pokazuje date w formacie miesiac/dzien/rok

        else:
            print("Wrong number, select from 0 to 2.")
            continue
        break

    task = Tasks(content_task, userDate)
    tasks.append(task)



def delete_task():
    task_index = int(input("Podaj indeks zadania do usunięcia: "))

    if task_index < 0 or task_index > len(tasks) - 1:
        print("Wybrałeś błędny index")
        return

    tasks.pop(task_index)
    print("Zadanie usunięte!")

    # Tutaj jest problem nie potrafię przypisać daty do zadania
    

def set_a_date():
    while True:
        try:
            year = int(input("Enter a year: "))
            month = int(input("Enter a month: "))
            day = int(input("Enter a day: "))
            userData = datetime.datetime(year, month, day) #ustawia czas wykonania na ten podany od użytkownika
            currentData = datetime.datetime.now() #czas obecny 



        #Można dodawać wiele exception pointów do try'a :
        except KeyError:
            print("Choose own date for this task")
            continue
        except ValueError:
            print("Type the correct year/month/day!")
            continue
        else:
            if userData < currentData: #validacja daty
                print("wrong data")
            else:
                break

    return userData

def specify_date():
    when = ["Today", "Tomarrow", "Other date"]
    while True:
        specify_date = input("Specify day and month: ")
        if specify_date == "day":
            for number, deadline in enumerate(when):
                print(f"{number}. Deadline for this task is{deadline}")
            while True:
                try:
                    choose_date = int(input(" Choose date: "))
                    if choose_date >len(when):
                        raise ValueError

                except:
                    print("It must be a number")
                    continue
                # if choose_date == 0



def save_tasks_to_file():
    file = open("tasks.txt", "w")
    for task in tasks:
        file.write(task + "\n")
    file.close()


def load_tasks_from_file():
    try:
        file = open("tasks.txt")

        for line in file.readlines():
            tasks.append(line.strip())

        file.close()
    except FileNotFoundError:
        return


load_tasks_from_file()

while user_choice != 6:
    if user_choice == 1:
        show_tasks()

    if user_choice == 2:
        add_task()

    if user_choice == 3:
        delete_task()

    if user_choice == 4:
        set_a_date()

    if user_choice == 5:
        save_tasks_to_file()

    print()
    print("1. Pokaż zadania")
    print("2. Dodaj zadanie")
    print("3. Usuń zadanie")
    print("4. Ustaw termin")
    print("5. Zapisz zmiany")
    print("6. Wyjdź")

    user_choice = int(input("Wybierz liczbę: "))
    print()


